# provider_app

a flutter project

Practice "Provider" function, "sharing the same state between different widgets"

> setState()

we just use it and then create `build` state more times.

![provider_diagram](assets/provider_diagram.png)
(image by CodeX)

## Note

- Create a class that extend `ChangeNotifier` and provide a space to data exchange

- Put `notifyListeners` to listen Function changing

- Take `ChangeNotifierProvider` (such as subscribers) to `Widget`

```Dart
MaterialApp(
      title: 'Flutter Demo',
      home: ChangeNotifierProvider(
        create: (BuildContext context) => <extended-class>,
        child: HomePage(),
      ),
```

- Get a variable from `Provider` and control data by it
  
``` dart
    // Class: TimeInfo
    var timeInfo = Provider.of<TimeInfo>(context, listen: false);

```

- Use `Consumer` to show something from `Provider` (equal to `setState`)
  
``` dart
    // Class: TimeInfo 
Consumer<TimeInfo>(
          builder: (context, data, child) {
            // setSate()
            return Text(
              data.getRemainingTime().toString(),
              style: TextStyle(fontSize: 72),
            );
          },
        ),
```
