import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider_app/time_info.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int initialVaue = 10;

  @override
  void initState() {
    super.initState();
    Timer.periodic(Duration(seconds: 1), (_) {
      var timeInfo = Provider.of<TimeInfo>(context, listen: false);
      timeInfo.updateRemainingTime();
    });
  }

  @override
  Widget build(BuildContext context) {
    print("-----build-----");
    return Scaffold(
      body: Center(
        child: Consumer<TimeInfo>(
          builder: (context, data, child) {
            return Text(
              data.getRemainingTime().toString(),
              style: TextStyle(fontSize: 72),
            );
          },
        ),
      ),
    );
  }
}
