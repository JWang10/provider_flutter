import 'package:flutter/material.dart';
// import 'package:flutter/foundation.dart';

class TimeInfo extends ChangeNotifier {
  int _remainingTime = 5;
  int getRemainingTime() => _remainingTime;

  void updateRemainingTime() {
    if (_remainingTime > 0) _remainingTime--;
    notifyListeners();
  }
}
